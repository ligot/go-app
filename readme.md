# go-app

> A simple go app to test various technologies

## Usage

To run the program

```sh
> make
```

To run the tests

```sh
> make test
```

## Technologies

- [Golang](#markdown-header-golang)
- [PostgreSQL](#markdown-header-postgresql)
- [Jenkins](#markdown-header-jenkins)
- [Vault](#markdown-header-vault)
- [Consul](#markdown-header-consul)
- [Sentry](#markdown-header-sentry)
- [Swagger](#markdown-header-swagger)
- [Observability](#markdown-header-observability)
    - [Prometheus](#markdown-header-prometheus)
    - [Jaeger](#markdown-header-jaeger)
    - [Loki](#markdown-header-loki)
- [Docker](#markdown-header-docker)
- [Makefile](#markdown-header-makefile)
- [VSCode](#markdown-header-vscode)

### Golang

[Go](https://golang.org/) is an open source programming language that makes it 
easy to build simple, reliable, and efficient software.

#### Libraries

- PostgreSQL driver: [pg](https://github.com/lib/pq)
- HTTP router: [chi](https://github.com/go-chi/chi/)
- CLI parser: [kingpin](https://github.com/alecthomas/kingpin)
- Logging: [logrus](https://github.com/sirupsen/logrus)
- Embed static files: [pkger](https://github.com/markbates/pkger)
- Docker tests: [dockertest](https://github.com/ory/dockertest)

#### Resources

* [Go for Industrial Programming](https://peter.bourgon.org/go-for-industrial-programming/)

### PostgreSQL

[PostgreSQL](https://www.postgresql.org/) is an open source relational database.

Some external user interfaces:
- [pgAdmin](https://www.pgadmin.org/) - a GUI for PostgreSQL
- [pgcli](https://www.pgcli.com/) - a CLI for PostgreSQL with auto-completion and syntax highlighting

Create a _demo_ database

```sh
> psql -d demo -f sql/schema.sql
```

And populate it with

```sh
> psql -d demo -f sql/demo.sql
```

### Jenkins

[Jenkins Pipeline](https://jenkins.io/doc/book/pipeline/) is a suite of plugins 
which supports implementing and integrating continuous delivery pipelines into 
Jenkins.

### Vault

[Vault](https://www.vaultproject.io/) is a project by HashiCorp to manage secrets and protect sensitive data.

We use it here to [dynamically generate PostgreSQL credentials](https://www.vaultproject.io/docs/secrets/databases/postgresql.html).

We have to define two environment variables for this to work:
```sh
> export VAULT_TOKEN="..."
> export VAULT_ADDR='http://127.0.0.1:8200' // Assume that Vault is running on local
```

### Consul

[Consul](https://www.consul.io/) is another project by HashiCorp to secure service networking.

We use it here as a [service registry](https://www.consul.io/discovery.html) and to [load balance traffic with Nginx](https://learn.hashicorp.com/consul/integrations/nginx-consul-template).

The Consul UI is available at [http://localhost:8500](http://localhost:8500).

#### Health check

Defining a [health check](https://www.consul.io/docs/agent/checks.html) will allow Consul to check the health of our service.
This is done here using an asynchronous health check library named [go-health](https://github.com/InVisionApp/go-health) that checks, every 30 seconds, that the PostgreSQL database is still reachable.

The health check is avaiable at the `/check` endpoint:

```sh
> curl -i localhost:8000/check
```

### Sentry

[Sentry](https://sentry.io) is a service to monitor and track errors.

To use it, we have to define one environment variable

```sh
> export SENTRY_DSN="https://...@sentry.io/..."
```

### Swagger

[Swagger](https://swagger.io/) is a collection of tools to document REST API.

It comes with a [specification](https://swagger.io/resources/open-api/) and a [user interface](https://swagger.io/tools/swagger-ui/) where we can visualize and interact with an API.
The UI is available at the endpoint `/swagger-ui`: http://localhost:8000/swagger-ui

The spec is automatically generated [based on code comments](https://goswagger.io/use/spec.html), thanks to the library [go-swagger](https://goswagger.io).

### Observability

#### Prometheus

[Prometheus](https://prometheus.io/) is a monitoring and alerting toolkit.
Our server expose metrics at the `/metrics` endpoint
```sh
> curl localhost:8000/metrics
```

#### Jaeger

[Jaeger](https://www.jaegertracing.io/) is an end-to-end distributed tracing solution.

The Jaeger UI is available at [http://localhost:16686/](http://localhost:16686/).

#### Loki

[Loki](https://grafana.com/oss/loki/) is a new log aggregation system inspired by Prometheus.

To use it, we have to start the loki server as well as an agent to collect logs:

```sh
> loki -config.file loki/loki.yaml
> promtail -config.file loki/promtail.yaml
```

Then, we can start our app in production mode with

```sh
> make run-prod
```

### Docker

[Docker](https://www.docker.com/) allows to run apps in containers.
This can be used to 
* spin up a PostgreSQL database for the tests, thanks to a library named [dockertest](https://github.com/ory/dockertest)
* develop inside a container in [Visual Studio Code](https://code.visualstudio.com/docs/remote/containers)

### Makefile

make is an old buil tool from the 70s that can be used as a [Golang test runner](https://sahilm.com/makefiles-for-golang/).

### VSCode

[Visual Studio Code](https://code.visualstudio.com/) is a source code editor developed by Microsoft.

#### Extensions

Here are some useful extensions:
- [Go](https://marketplace.visualstudio.com/items?itemName=ms-vscode.Go)
- [Jira and Bitbucket](https://marketplace.visualstudio.com/items?itemName=Atlassian.atlascode)
- [PostgreSQL](https://marketplace.visualstudio.com/items?itemName=ckolkman.vscode-postgres)
- [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
- [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
- [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
