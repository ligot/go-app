package tracing

import (
	"io"
	"net/http"

	"github.com/opentracing-contrib/go-stdlib/nethttp"
	"github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
	config "github.com/uber/jaeger-client-go/config"
	"github.com/uber/jaeger-lib/metrics/prometheus"
)

type logger struct {
	logger log.StdLogger
}

func (l *logger) Error(msg string) {
	l.logger.Printf("ERROR: %s", msg)
}

func (l *logger) Infof(msg string, args ...interface{}) {
	l.logger.Printf(msg, args...)
}

type Tracing struct {
	Tracer opentracing.Tracer
	Closer io.Closer
}

func New(service string, l log.StdLogger) (*Tracing, error) {
	metricsFactory := prometheus.New()
	cfg := &config.Configuration{
		ServiceName: service,
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans: true,
		},
	}
	tracer, closer, err := cfg.NewTracer(
		config.Logger(&logger{l}),
		config.Metrics(metricsFactory),
	)
	if err != nil {
		return nil, err
	}
	return &Tracing{tracer, closer}, nil
}

func (t *Tracing) Middleware(h http.Handler) http.Handler {
	return nethttp.Middleware(t.Tracer, h)
}
