package check

import (
	"database/sql"
	"net/http"
	"time"

	"github.com/InVisionApp/go-health"
	"github.com/InVisionApp/go-health/checkers"
	"github.com/InVisionApp/go-health/handlers"
)

type Check struct {
	hc *health.Health
}

func NewCheck(db *sql.DB) (check *Check, err error) {
	sqlCheck, err := checkers.NewSQL(&checkers.SQLConfig{
		Pinger: db,
	})
	if err != nil {
		return
	}

	hc := health.New()
	hc.DisableLogging()
	err = hc.AddCheck(&health.Config{
		Name:     "sql-check",
		Checker:  sqlCheck,
		Interval: time.Duration(30) * time.Second,
		Fatal:    true,
	})
	if err != nil {
		return
	}
	err = hc.Start()
	if err != nil {
		return
	}
	check = &Check{hc}
	return
}

func (check *Check) GetCheck(w http.ResponseWriter, r *http.Request) {
	h := handlers.NewJSONHandlerFunc(check.hc, nil)
	h(w, r)
}
