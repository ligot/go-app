package jwt

import (
	"fmt"
	"regexp"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Claims struct {
	jwt.StandardClaims
}

type Jwt struct {
	secret string
}

func New(secret string) *Jwt {
	return &Jwt{secret: secret}
}

func (j *Jwt) Parse(auth string) (*jwt.Token, error) {
	if auth == "" {
		return nil, fmt.Errorf("No Authorization found in the header")
	}
	re := regexp.MustCompile(`Bearer ([\w-.]+)`)
	matched := re.FindStringSubmatch(auth)
	if len(matched) != 2 {
		return nil, fmt.Errorf("Wrong format for the header %v", auth)
	}
	token, err := jwt.ParseWithClaims(matched[1], &Claims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(j.secret), nil
	})
	if err != nil {
		return nil, fmt.Errorf("Error while parsing the token: %v", err)
	}
	return token, nil
}

func (j *Jwt) New(user string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &jwt.StandardClaims{
		Subject:   user,
		ExpiresAt: time.Now().Add(time.Hour * 12).Unix(),
	})
	return token.SignedString([]byte(j.secret))
}
