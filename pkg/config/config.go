package config

import (
	"os"

	"gopkg.in/alecthomas/kingpin.v2"
)

type Config struct {
	Port       int
	Env        string
	Debug      bool
	VaultToken string
	VaultAddr  string
	JwtSecret  string
}

func New(version string) (config *Config, err error) {
	config = &Config{}
	app := kingpin.New("go-app", "A simple go app to test various technologies")
	app.Version(version)
	app.Flag("port", "Port number").Default("8000").IntVar(&config.Port)
	app.Flag("env", "Environment (dev or prod)").Default("dev").EnumVar(&config.Env, "dev", "prod")
	app.Flag("debug", "Debug").BoolVar(&config.Debug)
	app.Flag("vault_token", "Vault Token").Envar("VAULT_TOKEN").StringVar(&config.VaultToken)
	app.Flag("vault_addr", "Vault Address").Envar("VAULT_ADDR").Default("http://127.0.0.1:8200").StringVar(&config.VaultAddr)
	app.Flag("jwt_secret", "JWT Secret").Envar("JWT_SECRET").StringVar(&config.JwtSecret)
	kingpin.MustParse(app.Parse(os.Args[1:]))
	return
}
