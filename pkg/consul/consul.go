// Package consul is a small wrapper around the Consul API
// to (de-)register services
package consul

import (
	"fmt"

	"github.com/hashicorp/consul/api"
)

// Service represents a service in Consul
type Service struct {
	Name   string
	Port   int
	ID     string
	client *api.Client
}

// New creates a new Consul service
func New(name string, port int) *Service {
	return &Service{
		Name: name,
		Port: port,
	}
}

// Register the service in Consul
func (c *Service) Register() (err error) {
	c.client, err = api.NewClient(api.DefaultConfig())
	if err != nil {
		return
	}
	ID := fmt.Sprintf("%s%d", c.Name, c.Port)
	err = c.client.Agent().ServiceRegister(&api.AgentServiceRegistration{
		ID:   ID,
		Name: c.Name,
		Tags: []string{"primary"},
		Port: c.Port,
		Check: &api.AgentServiceCheck{
			HTTP:     fmt.Sprintf("http://localhost:%d/check", c.Port),
			Interval: "10s",
		},
	})
	if err != nil {
		return
	}
	c.ID = ID
	return
}

// Deregister the service
func (c *Service) Deregister() error {
	err := c.client.Agent().ServiceDeregister(c.ID)
	if err != nil {
		return err
	}
	c.ID = ""
	return nil
}

// IsRegistered tells if the service is registered in Consul
func (c *Service) IsRegistered() bool {
	return c.ID != ""
}
