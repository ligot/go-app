package vault

// Based on https://github.com/kelseyhightower/hashiapp/blob/master/vault.go

import (
	"errors"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/sirupsen/logrus"
)

type VaultClient struct {
	logger          logrus.StdLogger
	client          *api.Client
	dbLeaseID       string
	dbLeaseDuration int
	dbRenewable     bool
}

func NewVaultClient(addr, token string, logger logrus.StdLogger) (*VaultClient, error) {
	config := api.Config{Address: addr}
	client, err := api.NewClient(&config)
	if err != nil {
		return nil, err
	}
	client.SetToken(token)
	return &VaultClient{client: client, logger: logger}, nil
}

func (v *VaultClient) GetDatabaseCredentials(path string) (string, string, error) {
	secret, err := v.client.Logical().Read(path)
	if err != nil {
		return "", "", err
	}
	v.dbLeaseID = secret.LeaseID
	v.dbLeaseDuration = secret.LeaseDuration
	v.dbRenewable = secret.Renewable

	username := secret.Data["username"].(string)
	password := secret.Data["password"].(string)
	return username, password, nil
}

func (v *VaultClient) RenewDatabaseCredentials() error {
	if !v.dbRenewable {
		return errors.New("credentials not renewable")
	}

	v.logger.Println("Renewing credentials:", v.dbLeaseID)
	_, err := v.client.Sys().Renew(v.dbLeaseID, v.dbLeaseDuration)
	if err != nil {
		v.logger.Println(err)
	}

	// Renew the lease before it expires.
	duration := (v.dbLeaseDuration - 300)

	for {
		time.Sleep(time.Second * time.Duration(duration))
		v.logger.Println("Renewing credentials:", v.dbLeaseID)
		// Should we be reusing the secret?
		_, err := v.client.Sys().Renew(v.dbLeaseID, v.dbLeaseDuration)
		if err != nil {
			v.logger.Println(err)
			continue
		}
	}
}

func (v *VaultClient) GetJwtSecret(path string) (string, error) {
	secret, err := v.client.Logical().Read(path)
	if err != nil {
		return "", err
	}

	data := secret.Data["data"].(map[string]interface{})
	value := data["value"].(string)
	return value, nil
}
