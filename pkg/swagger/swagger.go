// Package swagger go-app API.
//
// API documentation for go-app
//
//     Schemes: http
//     Host: localhost:8000
//     BasePath: /
//     Version: 0.0.1
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: Olivier Ligot<oligot@protonmail.com> https://www.oligot.be/
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package swagger

import (
	"bufio"
	"fmt"
	"net/http"
	"regexp"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/markbates/pkger"
)

type Swagger struct {
	Port int
}

func (s *Swagger) getSwaggerSpec(w http.ResponseWriter, r *http.Request) {
	f, err := pkger.Open("/pkg/swagger/swagger.yaml")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	spec := ""
	re := regexp.MustCompile(`host: localhost:(\d)`)
	for scanner.Scan() {
		line := scanner.Text()
		matched := re.MatchString(line)
		if err != nil {
			panic(err)
		}
		if matched {
			portRe := regexp.MustCompile(`(\d)+`)
			line = portRe.ReplaceAllString(line, strconv.Itoa(s.Port))
		}
		spec += fmt.Sprintf("%s\n", line)
	}
	_, err = w.Write([]byte(spec))
	if err != nil {
		panic(err)
	}
}

func (s *Swagger) Register(r chi.Router) {
	assetHandler := http.FileServer(pkger.Dir("/assets/swagger-ui"))
	pkger.Include("./swagger/swagger.yaml")
	r.Get("/swagger.yaml", s.getSwaggerSpec)
	r.Get("/swagger-ui/*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs := http.StripPrefix("/swagger-ui/", assetHandler)
		fs.ServeHTTP(w, r)
	}))
}
