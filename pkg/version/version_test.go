package version

import (
	"net/http"
	"net/http/httptest"
	"testing"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func TestVersion(t *testing.T) {
	build := BuildInfo{Version: version, Commit: commit, Date: date}
	req, err := http.NewRequest(http.MethodGet, "/version", nil)
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	handler := http.HandlerFunc(build.GetVersion)
	handler.ServeHTTP(rec, req)

	expected := `{"version":"dev","commit":"none","date":"unknown"}
`
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, expected, rec.Body.String())
}
