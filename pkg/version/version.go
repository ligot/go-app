package version

import (
	"net/http"

	"github.com/go-chi/render"
)

type BuildInfo struct {
	Version string `json:"version"`
	Commit  string `json:"commit"`
	Date    string `json:"date"`
}

func (build *BuildInfo) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (build *BuildInfo) GetVersion(w http.ResponseWriter, r *http.Request) {
	if err := render.Render(w, r, build); err != nil {
		panic(err)
	}
}
