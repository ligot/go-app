package user

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	httpApp "go-app/pkg/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/go-testfixtures/testfixtures/v3"
	_ "github.com/lib/pq"
	"github.com/ory/dockertest/v3"
	"github.com/stretchr/testify/assert"
)

var db *sql.DB

func TestMain(m *testing.M) {
	// PostgreSQL docker container
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	resource, err := pool.Run("postgres", "12", []string{"POSTGRES_PASSWORD=secret", "POSTGRES_DB=user_test"})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	if err := pool.Retry(func() error {
		var err error
		db, err = sql.Open("postgres", fmt.Sprintf("postgres://postgres:secret@localhost:%s/user_test?sslmode=disable", resource.GetPort("5432/tcp")))
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to PostgreSQL docker container: %s", err)
	}

	// Create DB schema
	file, err := os.Open("../../sql/schema.sql")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	content, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	requests := strings.Split(string(content), ";")
	for _, request := range requests {
		_, err := db.Exec(request)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Load fixtures
	fixtures, err := testfixtures.New(
		testfixtures.Database(db),
		testfixtures.Dialect("postgres"),
		testfixtures.Directory("fixtures"),
	)
	if err != nil {
		log.Fatal(err)
	}
	if err := fixtures.Load(); err != nil {
		log.Fatal(err)
	}

	// Run tests
	code := m.Run()
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}
	os.Exit(code)
}

func TestGetUser(t *testing.T) {
	a := NewService(NewRepo(db), nil, 0)
	req, err := http.NewRequest(http.MethodGet, "/user/3", nil)
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Get("/user/{id}", a.GetUser)
	r.ServeHTTP(rec, req)
	expected := `{"id":3,"age":15,"firstName":"Jerry","lastName":"Seinfeld","email":"jerryjr123@gmail.com"}
`
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, expected, rec.Body.String())
}

func TestGetUserIdType(t *testing.T) {
	a := NewService(NewRepo(db), nil, 0)
	req, err := http.NewRequest(http.MethodGet, "/user/abc", nil)
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Get("/user/{id}", a.GetUser)
	r.ServeHTTP(rec, req)

	var res httpApp.ErrResponse
	err = json.Unmarshal(rec.Body.Bytes(), &res)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, "WRONG_PARAM_TYPE", res.Code)
}

func TestGetUserNotFound(t *testing.T) {
	a := NewService(NewRepo(db), nil, 0)
	req, err := http.NewRequest(http.MethodGet, "/user/10", nil)
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Get("/user/{id}", a.GetUser)
	r.ServeHTTP(rec, req)

	var e httpApp.ErrResponse
	err = json.Unmarshal(rec.Body.Bytes(), &e)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotFound, rec.Code)
	assert.Equal(t, "NOT_FOUND", e.Code)
}

func TestPostUser(t *testing.T) {
	a := NewService(NewRepo(db), nil, 0)
	userJSON := `{"age":25, "firstName": "John", "lastName": "Snow", "email": "john.snow@got.com"}`
	req, err := http.NewRequest(http.MethodPost, "/user", strings.NewReader(userJSON))
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Post("/user", a.PostUser)
	r.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusCreated, rec.Code)
	assert.Regexp(t, "http://localhost:0/user/[0-9]+", rec.Result().Header["Location"][0])
}

func TestPostUserEmailExists(t *testing.T) {
	a := NewService(NewRepo(db), nil, 0)
	userJSON := `{"age":25, "firstName": "John", "lastName": "Snow", "email": "jon@calhoun.io"}`
	req, err := http.NewRequest(http.MethodPost, "/user", strings.NewReader(userJSON))
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Post("/user", a.PostUser)
	r.ServeHTTP(rec, req)

	var res httpApp.ErrResponse
	err = json.Unmarshal(rec.Body.Bytes(), &res)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, "ALREADY_EXIST", res.Code)
}

func TestDeleteUser(t *testing.T) {
	a := NewService(NewRepo(db), nil, 0)
	req, err := http.NewRequest(http.MethodDelete, "/user/3", nil)
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Delete("/user/{id}", a.DeleteUser)
	r.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusNoContent, rec.Code)
}

func TestUpdateUser(t *testing.T) {
	a := NewService(NewRepo(db), nil, 0)
	userJSON := `{"id": 2, "age":25, "firstName": "John"}`
	req, err := http.NewRequest(http.MethodPatch, "/user", strings.NewReader(userJSON))
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Patch("/user", a.UpdateUser)
	r.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusNoContent, rec.Code)
}
