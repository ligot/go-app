package user

type User struct {
	ID        int32  `json:"id"`
	Age       int32  `json:"age"`
	FirstName string `json:"firstName" db:"first_name"`
	LastName  string `json:"lastName" db:"last_name"`
	Email     string `json:"email" validate:"required,email"`
	Password  string `json:"-"`
}

type CreateUser struct {
	Age       int32  `json:"age"`
	FirstName string `json:"firstName" db:"first_name"`
	LastName  string `json:"lastName" db:"last_name"`
	Email     string `json:"email" validate:"required,email"`
}

func (u *User) Merge(o *User) {
	if o.Age != 0 {
		u.Age = o.Age
	}
	if o.FirstName != "" {
		u.FirstName = o.FirstName
	}
	if o.LastName != "" {
		u.LastName = o.LastName
	}
	if o.Email != "" {
		u.Email = o.Email
	}
}
