package user

import (
	"context"
	"fmt"
	httpApp "go-app/pkg/http"
	"go-app/pkg/jwt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/go-playground/validator/v10"
)

type Service struct {
	repo *Repo
	jwt  *jwt.Jwt
	port int
}

// swagger:response UserResponse
type UserResponse struct {
	// in:body
	UserResponse User
}

// swagger:response empty
type EmptyResponse struct{}

// swagger:response notFound
type NotFound struct{}

// swagger:response parameterBodies
type ParameterBodies struct {
	// in:body
	CreateUserOption CreateUser
}

func (u *User) Bind(r *http.Request) error {
	return nil
}

func (u *CreateUser) Bind(r *http.Request) error {
	return nil
}

func (u *User) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// swagger:response BadRequest
type BadRequest struct {
	// in:body
	ErrResponse httpApp.ErrResponse
}

func NewService(repo *Repo, jwt *jwt.Jwt, port int) *Service {
	return &Service{
		repo: repo,
		jwt:  jwt,
		port: port,
	}
}

func (s *Service) authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := s.jwt.Parse(r.Header.Get("Authorization"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			return
		}
		ctx := context.WithValue(r.Context(), "token", token)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (s *Service) GetUser(w http.ResponseWriter, r *http.Request) {
	// swagger:operation GET /user/{id} user getUser
	// ---
	// summary: Get a single user
	// parameters:
	// - name: id
	//   in: path
	//   description: User id
	//   type: integer
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/UserResponse"
	//   "400":
	//     "$ref": "#/responses/BadRequest"
	//   "404":
	//     "$ref": "#/responses/notFound"
	ctx := r.Context()
	idVar := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idVar, 10, 32)
	if err != nil {
		msg := fmt.Sprintf("Id %v is not an integer", idVar)
		httpApp.MustRender(w, r, &httpApp.ErrResponse{
			HttpCode: http.StatusBadRequest,
			Code:     "WRONG_PARAM_TYPE",
			Message:  msg,
		})
		return
	}
	u, err := s.repo.Read(ctx, int32(id))
	switch err := err.(type) {
	case nil:
		httpApp.MustRender(w, r, u)
	case *NotFoundError:
		httpApp.MustRender(w, r, httpApp.NotFoundError(err))
	default:
		panic(err)
	}
}

func (s *Service) PostUser(w http.ResponseWriter, r *http.Request) {
	// swagger:operation POST /user user postUser
	// ---
	// summary: Create a user
	// parameters:
	// - name: body
	//   in: body
	//   schema:
	//     "$ref": "#/definitions/CreateUser"
	// responses:
	//   "201":
	//     "$ref": "#/responses/empty"
	//   "400":
	//     "$ref": "#/responses/BadRequest"
	ctx := r.Context()
	u := new(CreateUser)
	if err := render.Bind(r, u); err != nil {
		httpApp.MustRender(w, r, httpApp.ParseError(err))
		return
	}
	validate := validator.New()
	if err := validate.Struct(u); err != nil {
		httpApp.MustRender(w, r, &httpApp.ErrResponse{
			HttpCode: http.StatusBadRequest,
			Code:     "VALIDATION_ERROR",
			Message:  err.Error(),
		})
		return
	}
	id, err := s.repo.Write(ctx, u)
	if err != nil {
		if IsAlreadyExist(err) {
			httpApp.MustRender(w, r, &httpApp.ErrResponse{
				HttpCode: http.StatusBadRequest,
				Code:     "ALREADY_EXIST",
				Message:  err.Error(),
			})
			return
		}
		panic(err)
	}
	w.Header().Set("Location", fmt.Sprintf("http://localhost:%d/user/%d", s.port, id))
	w.WriteHeader(http.StatusCreated)
}

func (s *Service) DeleteUser(w http.ResponseWriter, r *http.Request) {
	// swagger:operation DELETE /user/{id} user deleteUser
	// ---
	// summary: Delete a single user
	// parameters:
	// - name: id
	//   in: path
	//   description: User id
	//   type: integer
	//   required: true
	// responses:
	//   "204":
	//     "$ref": "#/responses/empty"
	//   "400":
	//     "$ref": "#/responses/BadRequest"
	ctx := r.Context()
	idVar := chi.URLParam(r, "id")
	id, err := strconv.ParseInt(idVar, 10, 32)
	if err != nil {
		msg := fmt.Sprintf("Id %v is not an integer", idVar)
		httpApp.MustRender(w, r, &httpApp.ErrResponse{
			HttpCode: http.StatusBadRequest,
			Code:     "WRONG_PARAM_TYPE",
			Message:  msg,
		})
		return
	}
	err = s.repo.Delete(ctx, int32(id))
	if err != nil {
		panic(err)
	}
	w.WriteHeader(http.StatusNoContent)
}

func (s *Service) UpdateUser(w http.ResponseWriter, r *http.Request) {
	// swagger:operation PATCH /user user patchUser
	// ---
	// summary: Update a user
	// parameters:
	// - name: body
	//   in: body
	//   schema:
	//     "$ref": "#/definitions/User"
	// responses:
	//   "204":
	//     "$ref": "#/responses/noContent"
	//   "400":
	//     "$ref": "#/responses/BadRequest"
	//   "404":
	//     "$ref": "#/responses/notFound"
	ctx := r.Context()
	u := new(User)
	if err := render.Bind(r, u); err != nil {
		httpApp.MustRender(w, r, httpApp.ParseError(err))
		return
	}
	ru, err := s.repo.Read(ctx, u.ID)
	switch err := err.(type) {
	case nil:
		ru.Merge(u)
		validate := validator.New()
		if err := validate.Struct(ru); err != nil {
			httpApp.MustRender(w, r, &httpApp.ErrResponse{
				HttpCode: http.StatusBadRequest,
				Code:     "VALIDATION_ERROR",
				Message:  err.Error(),
			})
			return
		}
		err = s.repo.Update(ctx, ru)
		if err != nil {
			panic(err)
		}
		w.WriteHeader(http.StatusNoContent)
	case *NotFoundError:
		httpApp.MustRender(w, r, httpApp.NotFoundError(err))
	default:
		panic(err)
	}
}

func (s *Service) Register(r chi.Router) {
	ur := chi.NewRouter()
	ur.Use(s.authenticate)
	ur.Get("/{id}", s.GetUser)
	ur.Post("/", s.PostUser)
	ur.Delete("/{id}", s.DeleteUser)
	ur.Patch("/", s.UpdateUser)
	r.Mount("/user", ur)
}
