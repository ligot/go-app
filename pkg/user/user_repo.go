package user

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"

	dbPkg "go-app/pkg/user/db"

	"github.com/lib/pq"
)

func ToUser(d dbPkg.User) *User {
	var age int32
	if d.Age.Valid {
		age = d.Age.Int32
	}
	u := &User{
		ID:        d.ID,
		Age:       age,
		FirstName: fromNullString(d.FirstName),
		LastName:  fromNullString(d.LastName),
		Email:     d.Email,
		Password:  fromNullString(d.Password),
	}
	return u
}

func fromNullString(s sql.NullString) string {
	if s.Valid {
		return s.String
	}
	return ""
}

func toNullString(s string) sql.NullString {
	if s != "" {
		return sql.NullString{String: s, Valid: true}
	}
	return sql.NullString{Valid: false}
}

type NotFoundError struct {
	What     string
	Property string
	err      error
}

func (e *NotFoundError) Error() string {
	return fmt.Sprintf("%s with property %s not found; %s", e.What, e.Property, e.err)
}

type alreadyExistError struct {
	What  string
	Value string
	err   error
}

func (e *alreadyExistError) Error() string {
	return fmt.Sprintf("%s with value %s already exist; %s", e.What, e.Value, e.err)
}

func (e *alreadyExistError) AlreadyExist() bool {
	return true
}

// Opaque error, as explained in
// https://dave.cheney.net/2016/04/27/dont-just-check-errors-handle-them-gracefully
type alreadyExist interface {
	AlreadyExist() bool
}

func IsAlreadyExist(err error) bool {
	te, ok := err.(alreadyExist)
	return ok && te.AlreadyExist()
}

const uniqueViolationCode = "23505"

type Repo struct {
	q *dbPkg.Queries
}

func NewRepo(sqlDb *sql.DB) *Repo {
	return &Repo{dbPkg.New(sqlDb)}
}

func (r *Repo) Read(ctx context.Context, id int32) (user *User, err error) {
	u, err := r.q.GetUser(ctx, id)
	if err == sql.ErrNoRows {
		return nil, &NotFoundError{What: "user", Property: strconv.FormatInt(int64(id), 10), err: err}
	}
	return ToUser(u), nil
}

func (r *Repo) ReadByEmail(ctx context.Context, email string) (user *User, err error) {
	u, err := r.q.GetUserByEmail(ctx, email)
	if err == sql.ErrNoRows {
		return nil, &NotFoundError{What: "user", Property: email, err: err}
	}
	return ToUser(u), nil
}

func (r *Repo) Write(ctx context.Context, user *CreateUser) (id int32, err error) {
	id, err = r.q.CreateUser(ctx, dbPkg.CreateUserParams{
		Age:       sql.NullInt32{Int32: user.Age, Valid: true},
		FirstName: toNullString(user.FirstName),
		LastName:  toNullString(user.LastName),
		Email:     user.Email,
	})
	if e, ok := err.(*pq.Error); ok && e.Code == uniqueViolationCode {
		err = &alreadyExistError{What: "user", Value: user.Email, err: e}
	}
	return
}

func (r *Repo) Delete(ctx context.Context, id int32) error {
	return r.q.DeleteUser(ctx, id)
}

func (r *Repo) Update(ctx context.Context, user *User) error {
	u := dbPkg.UpdateUserParams{
		ID:        user.ID,
		Age:       sql.NullInt32{Int32: user.Age, Valid: true},
		Email:     user.LastName,
		FirstName: toNullString(user.FirstName),
		LastName:  toNullString(user.LastName),
		Password:  toNullString(user.Password),
	}
	err := r.q.UpdateUser(ctx, u)
	if err == sql.ErrNoRows {
		return &NotFoundError{What: "user", Property: strconv.FormatInt(int64(user.ID), 10), err: err}
	}
	return nil
}
