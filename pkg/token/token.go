package token

import (
	httpApp "go-app/pkg/http"
	"go-app/pkg/jwt"
	"go-app/pkg/user"
	"net/http"

	"github.com/go-chi/render"
	"golang.org/x/crypto/bcrypt"
)

type TokenRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (e *TokenRequest) Bind(r *http.Request) error {
	return nil
}

// swagger:response parameterBodies
type ParameterBodies struct {
	// in:body
	TokenRequest TokenRequest
}

type Service struct {
	userRepo *user.Repo
	jwt      *jwt.Jwt
}

func New(userRepo *user.Repo, jwt *jwt.Jwt) *Service {
	return &Service{
		userRepo: userRepo,
		jwt:      jwt,
	}
}

func (s *Service) PostToken(w http.ResponseWriter, r *http.Request) {
	// swagger:operation POST /token token postToken
	// ---
	// summary: Create a token
	// parameters:
	// - name: body
	//   in: body
	//   schema:
	//     "$ref": "#/definitions/TokenRequest"
	// responses:
	//   "201":
	//     "$ref": "#/responses/TokenResponse"
	//   "400":
	//     "$ref": "#/responses/BadRequest"
	ctx := r.Context()
	t := new(TokenRequest)
	if err := render.Bind(r, t); err != nil {
		httpApp.MustRender(w, r, httpApp.ParseError(err))
		return
	}
	u, err := s.userRepo.ReadByEmail(ctx, t.Email)
	switch err := err.(type) {
	case nil:
		err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(t.Password))
		if err != nil {
			httpApp.MustRender(w, r, &httpApp.ErrResponse{
				HttpCode: 400,
				Code:     "WRONG_PASSWORD",
			})
			return
		}
		token, _ := s.jwt.New(u.Email)
		_, _ = w.Write([]byte(token))
	case *user.NotFoundError:
		httpApp.MustRender(w, r, httpApp.NotFoundError(err))
	default:
		panic(err)
	}
}
