package http

import (
	"net/http"

	"github.com/go-chi/render"
)

type ErrResponse struct {
	HttpCode int    `json:"-"`
	Code     string `json:"code"`
	Message  string `json:"message"`
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HttpCode)
	return nil
}

func ParseError(err error) render.Renderer {
	return &ErrResponse{
		HttpCode: http.StatusBadRequest,
		Code:     "PARSE_ERROR",
		Message:  err.Error(),
	}
}

func NotFoundError(err error) render.Renderer {
	return &ErrResponse{
		HttpCode: http.StatusNotFound,
		Code:     "NOT_FOUND",
		Message:  err.Error(),
	}
}

// MustRender is like render.Render but panics if render.Render returns an error
func MustRender(w http.ResponseWriter, r *http.Request, v render.Renderer) {
	if err := render.Render(w, r, v); err != nil {
		panic(err)
	}
}
