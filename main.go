package main

import (
	"context"
	"database/sql"
	"fmt"
	"go-app/pkg/check"
	"go-app/pkg/config"
	"go-app/pkg/consul"
	"go-app/pkg/jwt"
	"go-app/pkg/logging"
	"go-app/pkg/swagger"
	"go-app/pkg/token"
	"go-app/pkg/tracing"
	"go-app/pkg/user"
	"go-app/pkg/vault"
	v "go-app/pkg/version"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	sentryhttp "github.com/getsentry/sentry-go/http"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"
	uuid "github.com/satori/go.uuid"

	log "github.com/sirupsen/logrus"
)

var (
	version = "dev"     // Filled by goreleaser
	commit  = "none"    // Filled by goreleaser
	date    = "unknown" // Filled by goreleaser
)

type secrets struct {
	dsn string
	jwt string
}

func secretManagement(logger log.StdLogger, errChan chan error, cfg *config.Config) (sec *secrets, err error) {
	sec = &secrets{
		dsn: "dbname=demo sslmode=disable",
		jwt: cfg.JwtSecret,
	}
	if cfg.VaultAddr != "" && cfg.VaultToken != "" {
		logger.Println("Getting database credentials...")
		vc, err := vault.NewVaultClient(cfg.VaultAddr, cfg.VaultToken, logger)
		if err != nil {
			return nil, err
		}

		username, password, err := vc.GetDatabaseCredentials("database/creds/go-app")
		if err != nil {
			return nil, err
		}
		sec.dsn += fmt.Sprintf(" user=%s password=%s", username, password)
		go func() {
			errChan <- vc.RenewDatabaseCredentials()
		}()

		s, err := vc.GetJwtSecret("kv/data/jwt-secret")
		if err != nil {
			return nil, err
		}
		sec.jwt = s
	} else if cfg.JwtSecret == "" {
		logger.Println("No JWT secret found, generating one...")
		u := uuid.NewV4()
		sec.jwt = u.String()
	}
	return
}

func database(logger log.StdLogger, dsn string) *sql.DB {
	logger.Println("Initializing database...")
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		logger.Fatal(err)
	}
	return db
}

func errorReporting(logger log.StdLogger, debug bool) {
	logger.Println("Initializing error reporting...")
	err := sentry.Init(sentry.ClientOptions{
		ServerName:       "go-app",
		Debug:            debug,
		AttachStacktrace: true,
	})
	if err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}
}

func initHTTP(logger log.StdLogger, cfg *config.Config, db *sql.DB, secret string, t *tracing.Tracing) http.Handler {
	logger.Println("Initializing http...")
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	if cfg.Env == "prod" {
		r.Use(logging.NewStructuredLogger())
	} else {
		r.Use(middleware.Logger)
	}
	r.Use(middleware.Recoverer)
	r.Use(cors.Default().Handler)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	sentryHandler := sentryhttp.New(sentryhttp.Options{
		Repanic: true,
	})
	r.Use(sentryHandler.Handle)
	r.Use(t.Middleware)
	s := swagger.Swagger{Port: cfg.Port}
	s.Register(r)
	userRepo := user.NewRepo(db)
	j := jwt.New(secret)
	userService := user.NewService(userRepo, j, cfg.Port)
	ts := token.New(userRepo, j)
	r.Post("/token", ts.PostToken)
	userService.Register(r)

	build := v.BuildInfo{Version: version, Commit: commit, Date: date}
	r.Get("/version", build.GetVersion)

	c, err := check.NewCheck(db)
	if err != nil {
		logger.Fatal(err)
	}
	r.Get("/check", c.GetCheck)

	r.Handle("/metrics", promhttp.Handler())
	return r
}

func register(logger log.StdLogger, port int) *consul.Service {
	logger.Println("Registering service in Consul...")
	c := consul.New("go-app", port)
	err := c.Register()
	if err != nil {
		logger.Printf("Error when registering service: %s", err)
	}
	return c
}

func signals(logger log.StdLogger, errChan chan error, c *consul.Service, srv *http.Server) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case err := <-errChan:
			if err != nil {
				logger.Fatal(err)
			}
		case s := <-signalChan:
			logger.Printf("Captured %v. Exiting...", s)
			if c.IsRegistered() {
				logger.Println("Deregistring service...")
				err := c.Deregister()
				if err != nil {
					logger.Println(err)
				}
			}
			ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
			logger.Println("Shutting down server...")
			err := srv.Shutdown(ctx)
			if err != nil {
				logger.Println(err)
			}
			cancel()
			os.Exit(0)
		}
	}
}

func main() {
	logger := log.New()
	logger.Out = os.Stdout
	cfg, err := config.New(version)
	if err != nil {
		logger.Fatal(err)
	}
	if cfg.Env == "prod" {
		logger.Formatter = &log.JSONFormatter{}
	}
	logger.WithFields(log.Fields{
		"version": version,
		"commit":  commit,
		"date":    date,
	}).Info("Starting go-app")

	t, err := tracing.New("go-app", logger)
	if err != nil {
		logger.Fatal(err)
	}
	defer t.Closer.Close()

	errChan := make(chan error, 10)

	sec, err := secretManagement(logger, errChan, cfg)
	if err != nil {
		logger.Fatal(err)
	}

	db := database(logger, sec.dsn)
	defer db.Close()

	errorReporting(logger, cfg.Debug)
	r := initHTTP(logger, cfg, db, sec.jwt, t)

	c := register(logger, cfg.Port)

	srv := http.Server{Addr: fmt.Sprintf(":%d", cfg.Port), Handler: r}
	logger.WithFields(log.Fields{
		"port": cfg.Port,
	}).Info("Server started")
	go func() {
		errChan <- srv.ListenAndServe()
	}()

	signals(logger, errChan, c, &srv)
}
