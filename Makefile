golangci-lint = ./bin/golangci-lint
goreleaser = ./bin/goreleaser
swaggercli = $(GOPATH)/bin/swagger

# Run the server
run:
	@go run main.go
.PHONY: run

# Run the server in production mode
run-prod:
	@go run main.go --env prod > log/go-app.log 2>&1
.PHONY: run-prod

$(golangci-lint):
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s v1.21.0

$(goreleaser):
	curl -sfL https://install.goreleaser.com/github.com/goreleaser/goreleaser.sh | sh

# Lint the source code
lint: $(golangci-lint)
	@echo "Running golangci-lint..."
	@go list -f '{{.Dir}}' ./... \
		| xargs $(golangci-lint) run
.PHONY: lint

# Run all tests
test: lint
	@echo "Running go test..."
	@go test -cover ./...
.PHONY: test

# Test coverage
cover:
	@go test -coverprofile=coverage.out ./...
	@go tool cover -html=coverage.out
.PHONY: cover

# Release a new version
release: $(goreleaser)
	$(goreleaser) --skip-publish --rm-dist
.PHONY: release

$(swaggercli):
	GO111MODULE=off go get -u github.com/go-swagger/go-swagger/cmd/swagger

# Generate Swagger spec
swagger: $(swaggercli)
	$(swaggercli) generate spec -o ./pkg/swagger/swagger.yaml --scan-models
.PHONY: swagger
